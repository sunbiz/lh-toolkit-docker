FROM tomcat:7-jre8

RUN wget https://github.com/jwilder/dockerize/releases/download/v0.3.0/dockerize-linux-amd64-v0.3.0.tar.gz
RUN tar -C /usr/local/bin -xzvf dockerize-linux-amd64-v0.3.0.tar.gz

RUN cd /usr/local/tomcat/webapps \
&& wget https://dl.bintray.com/librehealth/lh-toolkit-war/lh-toolkit.war

CMD ["dockerize","-wait","tcp://db:3306","-timeout","30s","catalina.sh","run"]
